import psycopg2
import db_lab_final_project.settings as settings


def connect_to_db():
    if settings.DB_PASS is None:
        connection = psycopg2.connect(f'dbname=db_final_lab user={settings.DB_USER}')
    else:
        connection = psycopg2.connect(f'dbname=db_final_lab user={settings.DB_USER} password={settings.DB_PASS}')

    return connection, connection.cursor()


def check_missing_data(data):
    missing_data = ""
    print(data)
    data_is_missing = False
    for key in data:
        if data[key] is None:
            data_is_missing = True
            missing_data += f'{key}, '

    missing_data = missing_data[:-2]  # remove ', ' at the end

    return data_is_missing, missing_data
