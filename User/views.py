import json
import os
from django.http import HttpResponse
from db_lab_final_project.utils import connect_to_db, check_missing_data


def login(request):
    if len(request.POST) == 0:
        file_path = os.sep.join(["web", "login.html"])
        login_file_handler = open(file_path, 'r')
        login_page = login_file_handler.readlines()
        login_file_handler.close()
        return HttpResponse(login_page)

    input_data = {
        'username': request.POST.get('username'),
        'password': request.POST.get('password'),
    }

    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse("error: {}, success:{}".format(missing_data, False))
        response['Access-Control-Allow-Origin'] = '*'
        return response
    connection, cursor = connect_to_db()
    cursor.execute(f"select signIn('{input_data['username']}', '{input_data['password']}');")
    connection.commit()
    json_result = json.loads(cursor.fetchone()[0].replace("\\", ''))

    if json_result['result'] == 'success':
        response = HttpResponse("message: {}, success:{}".format(json_result['msg'], True))
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse("error: {}, success:{}".format(json_result['err'], False))
        response['Access-Control-Allow-Origin'] = '*'
        return response


def sign_up(request):
    input_data = {
        'username': request.POST.get('username'),
        'password': request.POST.get('password'),
        'first_name': request.POST.get('first_name'),
        'last_name': request.POST.get('last_name'),
        'birth_date': request.POST.get('birth_date')  # format is YYYY-MM-DD, e.g. '2000-03-13'
    }

    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse("error: {}, success:{}".format(missing_data, False))
        response['Access-Control-Allow-Origin'] = '*'
        return response

    connection, cursor = connect_to_db()
    cursor.execute(f"select signUp('{input_data['username']}', '{input_data['password']}', '{input_data['first_name']}'"
                   f", '{input_data['last_name']}', '{input_data['birth_date']}'::date);")
    connection.commit()
    json_result = json.loads(cursor.fetchone()[0])

    if json_result['result'] == 'success':
        response = HttpResponse("message: {}, success:{}".format(json_result['msg'], True))
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse("error: {}, success:{}".format(json_result['err'], False))
        response['Access-Control-Allow-Origin'] = '*'
        return response
