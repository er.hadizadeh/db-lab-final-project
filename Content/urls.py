from django.urls import path
from . import views


urlpatterns = [
    path("", views.home, name='home-page'),
    path("content/newpost/", views.new_post, name='new post'),
    path("content/newcomment/", views.new_comment, name='new comment'),
    #todo get post title and use it as the name
    path("content/post/", views.get_post, name='post'),
    path("content/posts/", views.get_posts, name='post'),
]
