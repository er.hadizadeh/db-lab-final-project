import os
import time


def save_file(file):
    file_name = str(file)
    file_name = change_file_name_if_exists(file_name)
    destination_directory = f"media/{file_name}"

    if not os.path.exists('media'):
        os.mkdir('media')

    with open(destination_directory, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return destination_directory


def change_file_name_if_exists(file_name):
    if os.path.exists(f'media/{file_name}'):
        file_type = file_name.split('.')[-1]
        file_name_without_type = file_name.replace(f".{file_type}", "")

        current_time = int(time.time())
        current_time_hex = hex(current_time)[2:]  # without 0x at first
        file_name = f"{file_name_without_type}_{current_time_hex}.{file_type}"
    return file_name
