import json

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from Content.utils import save_file
from db_lab_final_project.utils import check_missing_data, connect_to_db


def home(request):
    return HttpResponse("Home page")



def new_post(request):
    input_data = {
        'token': request.POST.get('token'),
        'title': request.POST.get('title'),
        'content': request.POST.get('content'),
    }
    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse(f"error: {missing_data}, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response

    image = request.FILES.get('image')
    if image:  # if an image is uploaded
        file_name = str(image)
        file_type = file_name.split('.')[-1]
        if file_type == 'png' or file_type == 'jpg' or file_type == 'jpeg':
            file_destination = save_file(image)
            image = file_destination
    else:
        image = ""

    connection, cursor = connect_to_db()
    cursor.execute(
        f"select publishPost('\\{input_data['token']}', '{input_data['title']}', '{input_data['content']}', '{image}');"
    )
    connection.commit()
    json_result = json.loads(cursor.fetchone()[0])
    if json_result['result'] == 'success':
        response = HttpResponse(f"message: {json_result['msg']}, success: True")
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse(f"error: {json_result['err']}, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response




def new_comment(request):
    input_data = {
        'token': request.POST.get('token'),
        'postID': request.POST.get('postID'),
        'content': request.POST.get('content'),
    }
    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse("error: {}, success:{}".format(missing_data, False))
        response['Access-Control-Allow-Origin'] = '*'
        return response
    connection, cursor = connect_to_db()
    cursor.execute(
        f"select publishComment('\\{input_data['token']}', '{input_data['postID']}', '{input_data['content']}', '');")
    connection.commit()
    json_result = json.loads(cursor.fetchone()[0])
    if json_result['result'] == 'success':
        response = HttpResponse("message: {}, success:{}".format(json_result['msg'], False))
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse("error: {}, success:{}".format(json_result['err'], False))
        response['Access-Control-Allow-Origin'] = '*'
        return response


def get_post(request):
    input_data = {
        'post_id': request.POST.get('post_id'),

    }
    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse(f"error: {missing_data}, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response

    connection, cursor = connect_to_db()
    cursor.execute(
        f'select * from "Content" where "contentID"={int(input_data["post_id"])};')
    connection.commit()
    query_data = (cursor.fetchone())
    if query_data:

        json_dict = {
            'content_id': query_data[0],
            'user_id': query_data[1],
            'content_markup': query_data[2],
            'image': query_data[3],
            'datetime': str(query_data[4]),
            'like': query_data[5],
            'dislike': query_data[6],
            'views': query_data[7]
        }
        json_result = json.dumps(json_dict)
        response = HttpResponse(f"{json_result}, success: True")
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse(f"error: Post not found, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response


def get_posts(request):
    input_data = {
        'offset': request.GET.get('offset'),
        'limit': request.GET.get('limit'),

    }
    data_is_missing, missing_data = check_missing_data(input_data)
    if data_is_missing:
        response = HttpResponse(f"error: {missing_data}, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response

    offset = int(input_data['offset'])
    limit = int(input_data['limit'])
    connection, cursor = connect_to_db()
    cursor.execute(
        f'select * from "Post" ;')
    connection.commit()
    query_data = (cursor.fetchmany(100))
    if len(query_data) < offset + limit:
        limit = len(query_data) - offset
    if limit <= 0:
        response = HttpResponse(f" no posts in range was found, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response
    query_data = query_data[offset:offset + limit]

    if query_data:
        result_array = []
        for i, current_row_post in enumerate(query_data):
            cursor.execute(
                f'select * from "Content" where "contentID"={int(current_row_post[0])};')
            connection.commit()
            current_row = cursor.fetchone()
            cursor.execute(
                f'select username From "EndUser" Where "userID"={current_row[1]};')
            connection.commit()
            username = cursor.fetchone()
            print(username)
            print(current_row)
            result_array.append(({
                'content_id': current_row[0],
                'user_id': current_row[1],
                'content_markup': current_row[2],
                'image': current_row[3],
                'datetime': str(current_row[4]),
                'like': current_row[5],
                'dislike': current_row[6],
                'views': current_row[7],
                'title': current_row_post[1],
                'user_name': username
            }))
        # json_result = json.dumps(result_array)
        response = JsonResponse({"data": result_array})
        response['Access-Control-Allow-Origin'] = '*'
        return response
    else:
        response = HttpResponse(f"error: Post not found, success: False")
        response['Access-Control-Allow-Origin'] = '*'
        return response
