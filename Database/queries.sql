-- Senarios are tested and every day test data are inserted to DB here

-- Delete tables
DO $$ 
  DECLARE 
    r RECORD;
BEGIN
  FOR r IN 
    (
      SELECT table_name 
      FROM information_schema.tables 
      WHERE table_schema=current_schema()
    ) 
  LOOP
     EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.table_name) || ' CASCADE';
  END LOOP;
END $$ ;

-- 1 Testing user account functions

-- 1.1 Signing up some users

select signup('meehrbod', '12345678', 'mehrbod', 'forohar', '1999-03-13'::date);
select signup('fardiddd', '87654321', 'fardiddd', 'farokh', '2004-03-13'::date);
select signup('kavehhhh', '12344321', 'kavehhhh', 'fardid', '2006-03-13'::date);
select signup('aliiiiii', '12344321', 'ali', 'zaman', '2006-03-13'::date);

select * from "EndUser";


-- 1.2 Trying to violate contraints

-- Volates username > 7
select signup('mehrbod', '12345678', 'mehrbod', 'forohar', '1999-03-13'::date);

-- Volates passwrod > 7
select signup('fardiddd', '8765431', 'fardiddd', 'farokh', '2004-03-13'::date);

-- Volates age > 11
select signup('kavehhhh', '12344321', 'kavehhhh', 'fardid', '2015-03-13'::date);


-- 1.3 Trying to login and token generation
select signin('kavehhhh', '12344321');
select signin('fardiddd', '87654321');

select * from "Token";

-- 1.4 user search

-- 1.4.1 by username
select getUsersByUserName((select token::text from "Token" where "userID" = 3), '%kav%');

-- 1.4.2 by lastname
select getUsersByLastName((select token::text from "Token" where "userID" = 3), 'f_r%');

-- 2 post functions

-- 2.1 post a content
select publishPost((select token::text from "Token" where "userID" = 3), 'First Test', '<h1>Hello</h1>');
select publishPost((select token::text from "Token" where "userID" = 2), 'Intro post', 'Im fardid');
select * from "Content";

-- 2.2 get user's posts
select getUserPosts((select token::text from "Token" where "userID" = 3), 2);
select getUserPosts((select token::text from "Token" where "userID" = 2), 3);

-- 2.3 get a post
select getPost((select token::text from "Token" where "userID" = 3), 2);
select getPost((select token::text from "Token" where "userID" = 2), 1);
select * from "Post";
select * from "View";

-- 2.4 like a post
select likeContent((select token::text from "Token" where "userID" = 3), 2);
select * from "Like";

-- 2.5 dislike a post
select dislikeContent((select token::text from "Token" where "userID" = 3), 2);
select * from "Dislike";

-- 2.6 delete expression 

-- 3 comment functions
select clearExprssionOnContent((select token::text from "Token" where "userID" = 3), 2);
select * from "Dislike";

-- 3.1 publish a comment
select publishComment((select token::text from "Token" where "userID" = 3), 3, 'This is aweful', '');
select * from "Comment";

-- 3.2 get post comments 
select getPostComments((select token::text from "Token" where "userID" = 3), 3);

-- 3.3 like a comment
select likeContent((select token::text from "Token" where "userID" = 3), 4);

-- 3.4 dislike a comment
select dislikeContent((select token::text from "Token" where "userID" = 3), 13);


