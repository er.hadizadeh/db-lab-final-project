create or replace procedure getTokenUserID(inout uid int, in _token text)
language plpgsql
as $$
declare
begin
	select "userID" into uid from "Token" as tk where tk."token"::text = _token;
end$$ ;

create or replace procedure likeContent(in _contentID int, in _userID int)
language plpgsql
as $$
declare
begin
	if (exists(select * from "Like" where "contentID" = _contentID and "userID" = _userID)) then
		return;
	end if;
	
	delete from "Dislike" where "contentID" = _contentID and "userID" = _userID;
	
	insert into "Like"("contentID", "userID")
	values(_contentID, _userID);
end$$ ;

create or replace procedure dislikeContent(in _contentID int, in _userID int)
language plpgsql
as $$
declare
begin
	if (exists(select * from "Dislike" where "contentID" = _contentID and "userID" = _userID)) then
		return;
	end if;
		
	delete from "Like" where "contentID" = _contentID and "userID" = _userID;
		
	insert into "Dislike"("contentID", "userID")
	values(_contentID, _userID);
end$$ ;

create or replace procedure deleteExpression(in _contentID int, in _userID int)
language plpgsql
as $$
declare
begin
	delete from "Like" where "contentID" = _contentID and "userID" = _userID;
	delete from "Dislike" where "contentID" = _contentID and "userID" = _userID;
end$$ ;

create or replace procedure viewContent(in _contentID int, in _userID int)
language plpgsql
as $$
declare
begin
	if (exists(select * from "View" where "contentID" = _contentID and "userID" = _userID)) then
		return;
	end if;
	
	insert into "View"("contentID", "userID")
	values(_contentID, _userID);
end$$ ;


-- select * from "Content";

-- set cj = '';
-- call generateContentJSON(myvars.cj, 13);
-- select myvars.cj;

