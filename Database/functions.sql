-- FUNCTION 1. SHOWING RESULT MESSAGE IN JSON FORMAT
create or replace function resultMessage(isSuccess boolean, msg text)
returns text
language plpgsql
as $$
declare
begin
	if (isSuccess = true) then
		return row_to_json(t) from(
			select 'success' as result, msg as msg
		) t;
	else 
		return row_to_json(t) from(
			select 'fail' as result, msg as err
		) t;
	end if;	
end$$ ;


-- FUNCTION 2. SIGN UP
create or replace function signUp("_username" varchar(20),
                                  "_password" varchar(8),
                                  "_firstname" varchar(50),
                                  "_lastname" varchar(50),
                                  "_birthDate" date)
returns text
language plpgsql
as $$
declare
	returnMessage text default '';
	isSuccess bool default false;
begin
	if (length("_password") < 8 or length("_username") < 8) then
		returnMessage = 'at least password or username length is less than 8, ' || returnMessage;
	else 
		if ((now()::date - "_birthDate") < 12*365) then
			returnMessage = 'people younger than 12 can not signin :() , ' || returnMessage;
		else
			if (exists(select * from "EndUser" where "username" = "_username")) then
				returnMessage = 'username is not available' || returnMessage;
			else
				insert into "EndUser"("username", 
					  "passwordHash", 
					  "firstName", 
					  "lastName", 
					  "birthDate", 
					  "signUpDate")
				values ("_username", 
						sha256("_password"::bytea),
						"_firstname",
						"_lastname",
						"_birthDate",
						now()::date);
				returnMessage := 'User added successfully';
				isSuccess = true;
			end if;
		end if;
	end if;
	return resultMessage(isSuccess, returnMessage);
end$$ ;

-- FUNCTION 3. SIGN IN
create or replace function signIn("_username" varchar(20),
                                  "password" char(8))
returns text
language plpgsql
as $$
declare
	uid int default -1;
	tokenText bytea default null;
	returnMessage text default 'no such user';
	isSuccess bool default false;
begin
	select "userID" into uid from "EndUser"
		where "username" = "_username" and "passwordHash" = sha256("password"::bytea);
	if (uid <> -1) then
		select tk."token" into tokenText from "Token" as tk where tk."userID" = uid;
		if (tokenText is null) then
			select sha256(to_char(now(), 'dd-MM-yyyy;HH:MM:ss')::bytea || "_username"::bytea || "password"::bytea) into tokenText;
			insert into "Token"("userID", "token", "loginDate") 
			values (uid,
				tokenText,
				now()::date);
		end if;
		returnMessage := tokenText::text;
		isSuccess = true;
	end if;
	return resultMessage(isSuccess, returnMessage);
end$$ ;


-- FUNCTION 4. publishContent
create or replace function publishContent(_uid int, 
								 		  _contentMarkup text, _image text)
returns int
language plpgsql
as $$
declare
	uid int default -1;
	ctntID int default -1;
begin
	select "userID" into uid from "EndUser" where "userID" = _uid;
	if (uid > -1) then
		Select nextval(pg_get_serial_sequence('"Content"', 'contentID')) into ctntID;
		insert into "Content"("contentID",
							  "userID",
							  "markupContent",
		                      "image",
							  "composeDate",
							  "numLikes",
							  "numDislikes",
							  "numViews") 
					  values (ctntID,
							  uid, 
							  _contentMarkup,
					          _image,
							  now()::date,
							  0,
							  0,
							  0); 
	end if;
	
	return ctntID;
end$$ ;


-- FUNCTION 5. publishPost
create or replace function publishPost("token" text, 
								 	   "_title" varchar(50),
									   "_contentMarkup" text,
									   "_image" text)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	ctntID int default -1;
	returnMessage text default 'Invalid or expired';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		ctntID = publishContent(usid, "_contentMarkup", "_image");
		insert into "Post"("contentID",
						   "title") 
					values(ctntID,
						   "_title");
						   
		returnMessage := 'Post published';
		isSuccess = true;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

-- FUNCTION 6. publishComment
create or replace function publishComment("token" text, 
								 	   	  "postID" int,
									   	  "_contentMarkup" text,
									   	  "_image" text)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	ctntID int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists(select * from "Post" where "contentID" = "postID")) then
			ctntID = publishContent(usid, "_contentMarkup", "_image");
			insert into "Comment"("contentID", 
								  "postID") 
						   values(ctntID,
								  "postID");
			returnMessage := 'Comment published';
			isSuccess = true;
		else
			returnMessage := 'No such post';
			isSuccess = false;
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function getUsersByUserName("token" text, 
								 	 matchString text)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		select json_agg(t) from(
			select "userID" id, "username", "lastName", "firstName"
			from "EndUser" where "username" like matchString order by "username"
		) t into returnMessage;
		isSuccess = true;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function getUsersByLastName("token" text, 
								 	 matchString text)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		select json_agg(t) from(
			select "userID" id, "username", "lastName", "firstName"
			from "EndUser" where "lastName" like matchString order by "lastName"
		) t into returnMessage;
		isSuccess = true;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function getUserPosts("token" text, 
								 	  	_usID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		select json_agg(t) from(
			select "userID", "contentID", title, "composeDate"::date 	
			from "Post" inner join "Content" using("contentID") where "Content"."userID" = _usID
		) t into returnMessage;
		isSuccess = true;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function getPost("token" text, 
								 	  	pstID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists (select "contentID" from "Post" where "contentID" = pstID)) then
			select json_agg(t) from(
				select title, "markupContent" as contents, "image", "numLikes", "numDislikes", "numViews", "composeDate"::date
				from "Post" inner join "Content" using ("contentID") where "contentID" = pstID
			) t into returnMessage;
			
			call viewContent(pstID, usid);
			
			isSuccess = true;
		else
			returnMessage := 'no such post';
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function getPostComments("token" text, 
								 	  	pstID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists (select "contentID" from "Post" where "contentID" = pstID)) then
			select json_agg(t) from(
				select "markupContent" as contents, "numLikes", "numDislikes", "numViews", "composeDate"::date
				from "Comment" inner join "Content" using("contentID") where "postID" = pstID
			) t into returnMessage;
									
			isSuccess = true;
		else
			returnMessage := 'no such post';
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function likeContent("token" text, 
								 	  	cntID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists (select "contentID" from "Content" where "contentID" = cntID)) then
			call likeContent(cntID, usid);	
			isSuccess = true;
			returnMessage := 'like registered';
		else
			returnMessage := 'no such content';
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function dislikeContent("token" text, 
								 	  	cntID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists (select "contentID" from "Content" where "contentID" = cntID)) then
			call dislikeContent(cntID, usid);
			returnMessage := 'unlike registered';
			isSuccess = true;
		else
			returnMessage := 'no such content';
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

create or replace function clearExprssionOnContent("token" text, 
								 	  				cntID int)
returns text
language plpgsql
as $$
declare
	usid int default -1;
	returnMessage text default 'Invalid or expired token';
	isSuccess bool default false;
begin
	call getTokenUserID(usid, "token");
	if (usid > -1) then
		if (exists (select "contentID" from "Content" where "contentID" = cntID)) then
			call deleteExpression(cntID, usid);
			returnMessage := 'opinio deleted';
			isSuccess = true;
		else
			returnMessage := 'no such content';
		end if;
	end if;
	
	return resultMessage(isSuccess, returnMessage);
end$$ ;

-- TEST CASES

-- select json_agg(t) from(
-- 	select 'Success' as success, 'User added' as msg
-- ) t

-- select row_to_json(t) from(
-- 	select 'Success' as success, 'User added' as msg
-- ) t

-- functions' tests

-- select resultMessage(true, 'test');

-- select signin('meehrbod', '12345678', 'mehrbod', 'forohar', '2000-03-13'::date);

-- select login('meehrbod', '12345678');

-- select * from "token" as tk;

-- select * from "Content";

-- Select nextval(pg_get_serial_sequence('"Content"', 'contentID'));

-- select * from "EndUser";

-- select exists(select "username" from "EndUser" as ue where ue."username" = username);

-- select 'sdf' || 'Ali';

-- select (now()::date - '2000-03-13'::date)/365

-- select now()::date;

-- select to_char(now(), 'dd-MM-yyyy;HH:MM:ss')

-- SELECT sha256('hello world!');

-- SELECT sha256(to_char(now(), 'dd-MM-yyyy;HH:MM:ss')::bytea || 'aliname'::bytea || sha256('72364872364')::bytea);
