-- TRIGGER 1. INCREASE LIKE NUMBERS
create or replace function increase_like_numbers()
	returns trigger
	language 'plpgsql'
	volatile
as $body$
	declare current_like_numbers integer;
begin
	select "Content"."numLikes" into current_like_numbers
	from "Content"
	where "Content"."contentID" = new."contentID";

	update "Content"
    set "numLikes" = current_like_numbers + 1
    where "contentID" = new."contentID";

	return new;
end
$body$;

drop trigger if exists increase_like_numbers_trigger on "Like";
create trigger increase_like_numbers_trigger
	after insert on "Like"
	for each row
	execute procedure increase_like_numbers();

-- TRIGGER 2. DECREASE LIKE NUMBERS
create or replace function decrease_like_numbers()
	returns trigger
	language 'plpgsql'
	volatile
as $body$
	declare current_like_numbers integer;
begin
	select "Content"."numLikes" into current_like_numbers
	from "Content"
	where "Content"."contentID" = old."contentID";

	update "Content"
    set "numLikes" = current_like_numbers - 1
    where "contentID" = old."contentID";

	return new;
end
$body$;

drop trigger if exists decrease_like_numbers_trigger on "Like";
create trigger decrease_like_numbers_trigger
	after delete on "Like"
	for each row
	execute procedure decrease_like_numbers();

-- TRIGGER 3. INCREASE DISLIKE NUMBERS
create or replace function increase_dislike_numbers()
	returns trigger
	language 'plpgsql'
	volatile
as $body$
	declare current_dislike_numbers integer;
begin
	select "Content"."numDislikes" into current_dislike_numbers
	from "Content"
	where "Content"."contentID" = new."contentID";

	update "Content"
    set "numDislikes" = current_dislike_numbers + 1
    where "contentID" = new."contentID";

	return new;
end
$body$;

drop trigger if exists increase_dislike_numbers_trigger on "Dislike";
create trigger increase_dislike_numbers_trigger
	after insert on "Dislike"
	for each row
	execute procedure increase_dislike_numbers();

-- TRIGGER 4. DECREASE LIKE NUMBERS
create or replace function decrease_dislike_numbers()
	returns trigger
	language 'plpgsql'
	volatile
as $body$
	declare current_dislike_numbers integer;
begin
	select "Content"."numDislikes" into current_dislike_numbers
	from "Content"
	where "Content"."contentID" = old."contentID";

	update "Content"
    set "numDislikes" = current_dislike_numbers - 1
    where "contentID" = old."contentID";

	return new;
end
$body$;

drop trigger if exists decrease_dislike_numbers_trigger on "Dislike";
create trigger decrease_dislike_numbers_trigger
	after delete on "Dislike"
	for each row
	execute procedure decrease_dislike_numbers();


-- TRIGGER 5. INCREASE VIEW NUMBERS
create or replace function increase_view_numbers()
	returns trigger
	language 'plpgsql'
	volatile
as $body$
	declare current_view_numbers integer;
begin
	select "Content"."numViews" into current_view_numbers
	from "Content"
	where "Content"."contentID" = new."contentID";

	update "Content"
    set "numViews" = current_view_numbers + 1
    where "contentID" = new."contentID";

	return new;
end
$body$;

drop trigger if exists increase_view_numbers_trigger on "View";
create trigger increase_view_numbers_trigger
	after insert on "View"
	for each row
	execute procedure increase_view_numbers();
