drop owned by backend_service_group;
drop owned by backend;
drop owned by dbAdmin;
drop role backend;
drop role backend_service_group;
drop role dbAdmin;

create role backend_service_group with;
grant select, insert 
	on table "EndUser", "Comment", "Content", "Post", "Like", "Dislike", "View", "token"
	to group backend_service_group;
grant usage, select  
	on sequence "Content_contentID_seq"
	to group backend_service_group;
create role backend with password 'backendpass' nosuperuser nocreatedb nocreaterole login inherit;
grant backend_service_group to backend;

create role dbadmin with password 'adminpass' createrole login;
grant all privileges
	on database "Project"
	to group dbadmin;
