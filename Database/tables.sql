CREATE TABLE IF NOT EXISTS "EndUser" (
	"userID" serial,
	"username" varchar(20),
	"passwordHash" bytea,
	"firstName" varchar(50),
	"lastName" varchar(50),
	"birthDate" date,
	"signUpDate" date,
	PRIMARY KEY ("userID")
);
CREATE INDEX "Key" ON  "EndUser" ("username");

CREATE TABLE IF NOT EXISTS "Follow" (
	"followingUserID" int,
	"followedUserID" int,
	"followDate" date,
	
	PRIMARY KEY ("followingUserID", "followedUserID"),
	foreign key("followingUserID")
		REFERENCES "EndUser"("userID"),
	foreign key("followedUserID")
		REFERENCES "EndUser"("userID")
);

CREATE TABLE IF NOT EXISTS "Content" (
	"contentID" serial,
	"userID" int,
	"markupContent" text,
	"image" text,
	"composeDate" timestamp,
	"numLikes" int,
	"numDislikes" int,
	"numViews" int,
	PRIMARY KEY ("contentID"),
	foreign key("userID")
	    REFERENCES "EndUser"("userID")
);

CREATE TABLE IF NOT EXISTS "Like" (
	"contentID" int,
	"userID" int,
	primary key("contentID", "userID" ),
	foreign key ("userID")
	    references "EndUser"("userID"),
    foreign key ("contentID")
	    references "Content"("contentID")
);

CREATE TABLE IF NOT EXISTS "Dislike" (
	"contentID" int,
	"userID" int,
	primary key("contentID", "userID" ),
	foreign key ("userID")
	  references "EndUser"("userID"),
    foreign key ("contentID")
	    references "Content"("contentID")
);

CREATE TABLE IF NOT EXISTS "View" (
	"contentID" int,
	"userID" int,
	primary key("contentID", "userID" ),
	foreign key ("userID")
	  references "EndUser"("userID"),
    foreign key ("contentID")
	    references "Content"("contentID")
);

CREATE TABLE IF NOT EXISTS "Post" (
	"contentID" int ,
	"title" varchar(50),

	primary key("contentID"),
	foreign key("contentID")
		references "Content"("contentID")
);

CREATE TABLE IF NOT EXISTS "Comment" (
	"contentID" int,
	"postID" int,
	primary key("contentID"),
	foreign key ("postID")
	  references "Post"("contentID"),
    foreign key ("contentID")
	    references "Content"("contentID")
);

CREATE TABLE IF NOT EXISTS "Token" (
	"userID" int,
	"token" bytea,
	"loginDate" Date,
	primary key("userID"),
	foreign key ("userID")
	  references "EndUser"("userID")
);
