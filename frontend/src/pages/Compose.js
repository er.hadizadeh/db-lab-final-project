import React, {useEffect, useRef, useState} from 'react'
import { FormGroup, FormControl, InputLabel, Input, Typography, makeStyles, Button, Divider, Switch, FormControlLabel } from '@material-ui/core';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { useLocation } from 'react-router-dom';


import TextField from '@material-ui/core/TextField';
import addImg from '../assets/add.png';
export default function Compose(props){
    const [state, setState] = useState({
        token:"",
        title:"",
        content:"",
        image: null,
    })
    const location = useLocation();
    const history = useHistory();


    useEffect(() => {
        if (location.state){
            const newState = state;
            newState.token = location.state.token;
            setState({...newState})
        }

    },[])


    const [imageUrl, setImageUrl] = useState("");

    const [responseData, setResponseData ] = useState({

    })
    const fileInput = useRef(null)

//----------------------------------------------- data handling ----------------------------------------------------------//
const onSend = () => {
    let bodyFormData = new FormData();
    if(state.token !== ""){
        bodyFormData.append("token",state.token);
        bodyFormData.append("title",state.title);
        bodyFormData.append("content",state.content);
        bodyFormData.append("image",state.image);


        console.log(state)
        console.log(bodyFormData)

        axios({
            method: "post",
            url: "http://localhost:8000/content/newpost/",
            data: bodyFormData,
            header: { "Content-Type": "multipart/form-data",'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST,GET,PUT, DELETE'},

        })
            .then(response => response)

            .then(data => {
                console.log(data);
                setResponseData( data.data);
                if(data.data.includes("True")){
                    history.push('/news')

                }
            })



            .catch(err => {
                console.error(err);

            });
    }
    else setResponseData("login again")

      };



//---------------------------------------- rendering ----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------


//-------------------------------- input handling -----------------------------------------------------------------

    const onFileChange = (e) => {
        // Update the state
        let newState = state;
        newState.image = e.target.files[0];
        setState({...newState} );
        e.target.files[0] ? setImageUrl(URL.createObjectURL(e.target.files[0])):setImageUrl(imageUrl)


    };
    const handleSubject = (e) => {
        var newState =  state;
        newState.title = e.target.value;
        setState({...newState}) ;
    }
    const handleBody = (e) => {
        var newState =  state;
        newState.content = e.target.value;
        setState({...newState}) ;
    }

    const handleReceivers = (e) => {
        var newArray = e.target.value.split(',').map((item) => item.replace("@foofle.com",""));
        while (newArray.length < 3)
            newArray.push("")
        var newState =  state;
        newState.receivers = newArray ;
        setState({...newState}) ;
    }

    const handleCCReceivers = (e) => {
        var newArray = e.target.value.split(',').map((item) => item.replace("@foofle.com",""));
        while (newArray.length < 3)
            newArray.push("")
        var newState =  state;
        newState.ccReceivers = newArray ;
        setState({...newState}) ;
    }

    console.log(state)

//----------------------------------------------------------------------------------------------------------------------------------
//style
const classes = useStyles();
 //------------------------------------------- main ----------------------------------------------------------------------------------
    return(
      <div className={classes.root}>
         <FormGroup className={classes.root}>
            <FormGroup className={classes.formGroup}>
                <FormControl>
                    { state.image ? <img className={classes.img} src={imageUrl}
                                         onClick={() => fileInput.current.click()}
                    /> :
                        <img className={classes.img} src={addImg}                         onClick={() => fileInput.current.click()}
                             onClick={() => fileInput.current.click()}
                        />}
                </FormControl>

                    <FormControl className={classes.fields}>
                        <TextField
                            id="outlined-textarea"
                            label="عنوان"
                            size="small "
                            placeholder="Placeholder"

                            variant="outlined"
                            placeholder="عنوان را وارد کنید"
                            onChange={handleSubject}

                        />
                    </FormControl>



                    <FormControl className={classes.fields}>
                        <TextField
                            id="body"
                            label="  متن پیغام خود  را وارد کنید"
                            multiline
                            rows={5}
                            //defaultValue="Default Value"
                            // variant="filled"
                            variant="outlined"
                            onChange={handleBody}
                            size="large "
                        />
                </FormControl>

                <FormControl className={classes.fields}>
                    <input type="file" onChange={onFileChange} className={classes.input} ref={fileInput}/>
                </FormControl>
                    <Button className={classes.btn} onClick={() => onSend()}> ارسال</Button>

                <FormControl className={classes.response}>
                    <Typography >
                        {JSON.stringify(responseData)   }
                    </Typography>
                </FormControl>

            </FormGroup>
        </FormGroup>


    </div>

                )
} ;

const mainWidth = window.innerWidth;
const mainHeight = window.innerHeight;

const useStyles = makeStyles({
    root:{
        flex:1,
        alignItems:"center"
    },
    formGroup: {
        flex:1,
        flexDirection:"column",
        alignSelf:"center",
        justifyContent:"space-between",
        width:mainWidth/4,
        borderRadius:25,
        color:"white",
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        margin:mainWidth/200,
        boxShadow: " 4px 4px 20px white",
        border:"0.1px solid white",
        overflow:"hidden"



    },
    btn: {
        background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
        borderRadius: 3,
        color: 'white',
        height: 48,
        padding: '20px 30px',
        alignSelf:"center",
        marginBottom:15,
        boxShadow: " 0px 0px 10px white",
        border:"0.1px solid white"

      },
      response:{
        textAlign:"center",
        textOverflow:"clip",
        width:mainWidth/4,
        color:"white",
        marginTop:2,
        marginBottom:2,
        overflow:"hidden"

      },

      fields: {
          flex: 1,
          marginRight: 1,
          marginLeft: 1,
          marginBottom: 4,
          direction: "rtl",
          color: "white",
          paddingRight: mainWidth / 100,
          paddingLeft: mainWidth / 100
      },
    img:{
        width:mainWidth / 4,
        height: mainWidth / 4 ,
        boxShadow: '0 0 10 rgba(0,0,0,01)',
        paddingBottom : "10px",
        alignSelf: 'center'
    },
    input: {visibility:"hidden"},




  });
