import React, {useEffect, useState } from 'react'
import { FormGroup, FormControl, InputLabel, Input, makeStyles, Button, Switch, FormControlLabel, Divider    } from '@material-ui/core';
import axios from 'axios';
import { useHistory } from 'react-router-dom';



export default function Signup(){
    const [state, setState] = useState({
        user_name:"",
        password:"",
        firs_name:"" ,
        last_name:"",
        birth_date:""
    })
    const history = useHistory();
    const [showResponse, setRenderResponseData] = useState(false);

    const [responseData, setResponseData ] = useState({})

    const changeUsernameInput = (e) => {
        setState({...state,user_name:e.target.value})
    }

    const changePasswordInput = (e) => {
        setState({...state,password:e.target.value})
    }



    const changeFNameInput = (e) => {
        setState({...state,firs_name:e.target.value})
    }

    const changeLNameInput = (e) => {
        setState({...state,last_name:e.target.value})
    }

    const changeBDInput = (e) => {
        setState({...state,birth_date:e.target.value})
    }


    const renderResponse = () => {
        if(showResponse){
            return(
                <div>
                    {JSON.stringify(responseData )}
                </div>
            )
        }
    }

    const handleSubmit = () => {
        setRenderResponseData(true)
        let bodyFormData = new FormData();
        bodyFormData.append("username",state.user_name);
        bodyFormData.append("password",state.password);
        bodyFormData.append("first_name",state.firs_name);
        bodyFormData.append("last_name",state.last_name);
        bodyFormData.append("birth_date",state.birth_date);
        console.log(state)
        console.log(bodyFormData)

        axios({
            method: "post",
            url: "http://localhost:8000/User/signup/",
            data: bodyFormData,
            header: { "Content-Type": "multipart/form-data",'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST,GET,PUT, DELETE'},

        })
            .then(response => response)

            .then(data => {console.log(data);
                setResponseData(data)
                if(data.data.includes("True"))
                    history.push('/login')
                else
                    setResponseData(data.data)
            })

            .catch(err => {
                console.error(err);
                setResponseData(err.message)
            });
    };



//style
    const classes = useStyles();

    return(
        <div className={classes.root}>
            <FormGroup className={classes.root}>
                <FormGroup  className={classes.formGroup}>
                    <h1>اطلاعات کاربری </h1>
                    <FormControl>

                        <InputLabel htmlFor="username"> نام کاربری</InputLabel>
                        <Input
                            id="username"
                            aria-describedby="my-helper-text"
                            onChange={changeUsernameInput}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel htmlFor="pass">رمز عبور </InputLabel>
                        <Input
                            id="pass"
                            aria-describedby="my-helper-text"
                            onChange={changePasswordInput}
                        />
                    </FormControl>

                    <Divider/>
                    <h1> اطلاعت شخصی </h1>
                    <FormControl>
                        <InputLabel style={{flex:1}} htmlFor="name">نام </InputLabel>
                        <Input
                            id="name"
                            aria-describedby="my-helper-text"
                            onChange={changeFNameInput}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel  style={{flex:1}} htmlFor="lName">نام خانوادگی </InputLabel>
                        <Input
                            id="Name"
                            aria-describedby="my-helper-text"
                            onChange={changeLNameInput}
                        />
                    </FormControl>



                    <FormControl>
                        <InputLabel htmlFor="address"> تاریخ تولد </InputLabel>
                        <Input
                            id="address"
                            aria-describedby="my-helper-text"
                            onChange={changeBDInput}
                        />
                    </FormControl>




                    <Button className={classes.btn} onClick={() => handleSubmit()}> ثبت اطلاعات</Button>

                    <FormControl className={classes.response}>
                        {renderResponse()}
                    </FormControl>

                </FormGroup>
            </FormGroup>



        </div>



    )
} ;


const mainWidth = window.innerWidth;
const mainHeight = window.innerHeight;
const useStyles = makeStyles({
    root:{
        flex:1,
        alignItems:"center"
    },
    formGroup: {
        flex:1,
        flexDirection:"column",
        justifyContent:"cneter",
        alignItems:"center",
        alignSelf:"center",
        width:mainWidth/2,
        borderRadius:25,
        color:"white",
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        marginTop:mainWidth/200,
        marginBottom:mainWidth/16.5,
        boxShadow: " 4px 4px 20px white",
        border:"0.1px solid white"
    },
    btn: {
        background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
        borderRadius: 3,
        color: 'white',
        height: 48,
        padding: '20px 30px',
        alignSelf:"center",
        marginTop:15,
        marginBottom:5,
        boxShadow: " 2px 2px 14px white",
        border:"0.1px solid white"
    },
    response:{
        textAlign:"center",
        textOverflow:"clip",
        width:mainWidth/2,
        color:"white",
        overflow:"hidden",
        marginTop:2,
        marginBottom:20
    }



});
