import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import axios from 'axios';



export default function  MailBox () {
    const [news, setNews] = useState([]);
    const [expanded, setExpanded] = useState([]);
    const [page, setPage] = useState(1);
    const classes = useStyles();

// ############################################ data handling #################################################
    // initializing data
    useEffect(() => {
        getMails();
    }, [page,news.length]);

    const initializeExpanded = (length) => {
        let exp = Array(length);
        exp.fill(false, 0, length);
        setExpanded([...exp]);

    }

// user data
    const getMails = () => {

        let bodyFormData = new FormData();


        axios({
            method: "POST",
            url: `http://localhost:8000/content/posts/?offset=${(page-1)*4}&limit=4`,
            header: {
                "Content-Type": "multipart/form-data", 'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST,GET,PUT, DELETE'
            },

        })
            .then(response => response)

            .then(data => {
                console.log(data.data.data)
                setNews([...data.data.data])


            })

            .catch(err => {
                console.error(err);

            });
    }


//##################################################### rendering ############################################################
    const renderMail = (item, index) => {
        const imgPath = item.image ? `../../../${item.image}` : "../assets/add.png";


        return (
            <div className={classes.card}>
                <img src={imgPath} width={mainWidth*0.208} height={mainWidth*0.208} />
                <div className={classes.data}>
                    <div className={classes.heading}>کاربر:   {item.user_name}&nbsp;&nbsp;</div>

                    <div className={classes.txtTitle} >
                          عنوان: {item.title}
                        <hr style={{width:mainWidth * 0.18, height:"1px"}}/>
                    </div>

                    <div className={classes.txtBody}>

                        <span>
                            &nbsp; میگه:
                        </span>

                        <br/>&nbsp;&nbsp;&nbsp;{item.content_markup}
                    </div>

                </div>
            </div>

        )

    }

//##################################################### main ###########################################################
    return (
        <div classes={classes.main}>
            <div className={classes.root}>
                {news.map((item, index) => renderMail(item, index))}
            </div>
            <Pagination className={classes.pagination} count={10} color="secondary"
                        onChange={(event, page) => setPage(page)}/>
        </div>
    );

}

 const mainWidth = window.innerWidth;
 const mainHeight = window.innerHeight;
 const useStyles = makeStyles((theme) => ({
    root: {
        display: "grid",
        gridTemplateColumns: "3fr 3fr 3fr 3fr",
        gridTemplateRows: "3fr",
        height:mainHeight*0.8,
        marginBottom: mainHeight * 0.02
    },


    heading: {
        color:"rgba(255,255,255,1)",
        textShadow:"0px 0px 1px black",
        fontSize:"20px",
        fontFamily:"cursive",
        fontWeight:"bold",
        textAlign:"right",
        direction:'rtl'


    },
    txtTitle: {
        color:"rgba(255,255,255,1)",
        textShadow:"0px 0px 1px black",
        direction:"rtl",
        fontSize:"20px",
        fontFamily:"cursive",
        fontWeight:"bold",
        textAlign:"right",
        marginRight:"20px"
    },
     txtBody:{
         color:"rgba(255,255,255,1)",
         textShadow:"0px 0px 1px black",

         direction:"rtl",
         fontSize:"20px",
         fontFamily:"cursive",
         fontWeight:"bold",
         textAlign:"right"
     },

    main:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        alignContent:"center",
        border:"2px solid white",
        background:"#000"
    },

    pagination:{
      marginRight:window.innerWidth/4,
      marginLeft:window.innerWidth/3,
      paddingBottom:mainHeight/40
    },

    body:{
      flex:1,
      flexDirection:"column",
      direction:"rtl"

    },
     data:{
         display: "grid",
         gridTemplateColumns: "3fr",
         gridTemplateRows: "0.5fr 3fr",
         gap:"0px 0px"
},


     title:{
       visibility:"hidden"
     },
  card:{
      display:"flex",
      flexDirection:"column",
      alignItems:"flex-end",
      marginLeft: mainWidth * 0.02,
      marginRight: mainWidth * 0.02,
      borderRadius:25,
      boxShadow: " 0px 0px 10px white",
      border:"0.1px solid white",
      overflow:"hidden"
  }
  }));
